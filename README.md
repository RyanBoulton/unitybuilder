Hey, hi. This is the setup I use to automate making builds in Unity3D

Use it to automate exporting a combination of Windows, Mac & Linux builds.
If you want it to, you can select to compress these builds to .zip archives.
These can get sent to a separate directory (Like your local GoogleDrive/Dropbox folder)

It uses [DotNetZipForUnity](https://github.com/r2d2rigo/dotnetzip-for-unity)

I built this off of the back of Robert Yangs [UnityBuildAndZip](https://gist.github.com/radiatoryang/b65e9c4807a64987aba2)

[He makes good things.](https://twitter.com/radiatoryang)

#Main changes#

* Removed data folder copying (I don't use it for anything)
* Provided setup wizard
* Changed naming conventions for personal preference
* Got the DotNetZip compression progress bar to update correctly

#How To Use#

* Place Editor/UnityBuilder/UnityBuilderEditor.cs in your Editor folder.
* Place UnityBuilder.cs somewhere else.

* You'll find the setup wizard under Utilities->UnityBuilder
* Preferences selected here will be saved to Editor/UnityBuilder/UnityBuilderSettings.txt
* Just use 'Run..' if you want to use your saved settings without dealing with the settings window

* File name is taken from ProductName in Player Settings.

* Files are output in the following convention.
* <path>/<Filename+platform>/<Filename_DateSuffix>/<Filename_Platform>.<ext>
* "C:/Builds/Paperbark_Windows/Paperbark_WIN-11-16-2015/Paperbark_WIN.exe"

# #

I think that's everything.
I hope it works okay for you.

-Ryan Boulton [@HalfHealthDev](https://twitter.com/HalfHealthDev)