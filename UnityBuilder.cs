using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using Ionic.Zip; // this uses the Unity port of DotNetZip https://github.com/r2d2rigo/dotnetzip-for-unity

public class UnityBuilderSettings
{
    public bool BuildWin;
    public bool BuildMac;
    public bool BuildLinux;
    public string BuildPath = UnityBuilder.DEFAULT_BUILD_PATH;
    public bool ExportZip;
    public string ExportPath = UnityBuilder.DEFAULT_BUILD_PATH;
}

// place in an "Editor" folder in your Assets folder
public class UnityBuilder
{
    private const string SETTINGS_FILENAME = "UnityBuilderSettings.txt";
    static private string SETTINGS_PATH { get { return Application.dataPath + "/Editor/UnityBuilder/"; } }
    public static string DEFAULT_BUILD_PATH { get { return Application.dataPath.Replace("/Assets", "/Builds/"); } }

    private static string FileName { get { return Application.productName == "" ? "PRODUCT_NAME" : Application.productName; } }
    public static string SavePath { get { return ReadSettings().BuildPath; } }
    public static string ZipExportPath { get { return ReadSettings().ExportPath; } }

    #region Setup
    //Set ZIP Export Path
    public static string PromptForZipExportPath()
    {
        //Prompt user for path
        var path = EditorUtility.SaveFolderPanel("Export zip archives to..", Application.dataPath.Replace("/Assets", "/"), "");
        //If user hits cancel, doesn't apply new path
        if (path == "") return ""; 
        //Add end slash to directory
        path += "/";
        //Print the export path to console
        Debug.Log("[UnityBuilder]--> .zip Export Path set to '" + path + "'");
        return path;
    }

    //The way this reads data from a file is probably gross and wrong, but it works
    public static UnityBuilderSettings ReadSettings()
    {
        var settings = new UnityBuilderSettings();
        //Check for file
        if (File.Exists(SETTINGS_PATH + SETTINGS_FILENAME))
        {
            //Read file, remove line breaks and split contents
            var sr = new StreamReader(SETTINGS_PATH + SETTINGS_FILENAME);
            var data = sr.ReadToEnd();
            var fileContents = data.Replace("\n","").Split('[','=').ToList();
            sr.Close();

            //Cleanup
            var values = new List<string>();
            for (int i = 1; i < fileContents.Count; i++)
            {
                //Remove
                if (fileContents[i].Contains("]")) continue;
                //Keep
                values.Add(fileContents[i]);
            }
            //Parse & apple values
            if (fileContents.Count >= 5)
            {
                settings.BuildWin = (int.Parse(values[0]) == 1);
                settings.BuildMac = (int.Parse(values[1]) == 1);
                settings.BuildLinux = (int.Parse(values[2]) == 1);
                settings.ExportZip = (int.Parse(values[3]) == 1);
                settings.BuildPath = values[4].Replace("/n", "");
                settings.ExportPath = values[5];
                return settings;
            } 
        }
        //Launch default window if no builds selected
        if (!settings.BuildWin && !settings.BuildMac && !settings.BuildLinux)
        {
            UnityBuilderEditor.ShowWindow();
            return settings;
        }
        //Return settings
        return settings;
    }

    public static void WriteSettings(UnityBuilderSettings settings)
    {
        if (settings == null) settings = new UnityBuilderSettings();
        //If UnityBuilder fodler doesn't exist in the Editor folder, make one.
        if (Directory.Exists(SETTINGS_PATH) == false) Directory.CreateDirectory(SETTINGS_PATH);
        //Write path to text document
        var sw = new StreamWriter(SETTINGS_PATH+SETTINGS_FILENAME);
        sw.Write("[Build Windows] ={0}\n", settings.BuildWin ? 1 : 0);
        sw.Write("[Build OSX] ={0}\n", settings.BuildMac ? 1 : 0);
        sw.Write("[Build Linux] ={0}\n", settings.BuildLinux ? 1 : 0);
        sw.Write("[Do Zip Export] ={0}\n", settings.ExportZip ? 1 : 0);
        sw.Write("[Save Build Path] ={0}\n", settings.BuildPath ?? DEFAULT_BUILD_PATH);
        sw.Write("[Zip Export Path] ={0}\n", settings.ExportPath ?? DEFAULT_BUILD_PATH);
        sw.Close();
        //Reimport settings file to keep it up to date
        AssetDatabase.ImportAsset("Assets/Editor/UnityBuilder/"+SETTINGS_FILENAME);
    }
    #endregion

    #region BuildOptions
    //Build players with existing settings
    [MenuItem("Utilities/UnityBuilder/Run...")]
    public static void MakeBuilds()
    {
        var settings = ReadSettings();
        if (settings.BuildWin) StartWindows(settings.ExportZip);
        if (settings.BuildMac) StartOSX(settings.ExportZip);
        if (settings.BuildLinux) StartLinux(settings.ExportZip);
    }

    //____________BUILD WINDOWS____________//
    public static void StartWindows(bool exportZip)
    {
        BuildPlayer(BuildTarget.StandaloneWindows, FileName, SavePath + FileName + "_Windows", exportZip);
    }

    //____________BUILD OSX____________//
    public static void StartOSX(bool exportZip)
    {
        BuildPlayer(BuildTarget.StandaloneOSXUniversal, FileName, SavePath + FileName + "_OSX", exportZip);
    }

    //____________BUILD OSX____________//
    public static void StartLinux(bool exportZip)
    {
        BuildPlayer(BuildTarget.StandaloneLinuxUniversal, FileName, SavePath + FileName + "_Linux", exportZip);
    }
    #endregion

    static void BuildPlayer(BuildTarget buildTarget, string filename, string path, bool exportZip)
    {
        var fileExtension = "";
        var modifier = "";
        var dateSuffix = "-" + DateTime.Today.ToShortDateString().Replace("/", "-");

        //Clear staging area
        ClearStagingArea();

        //Determine modifiers and extensions based on target platform
        switch (buildTarget)
        {
            case BuildTarget.StandaloneWindows:
            case BuildTarget.StandaloneWindows64:
                modifier = "_WIN";
                fileExtension = ".exe";
                break;
            case BuildTarget.StandaloneOSXIntel:
            case BuildTarget.StandaloneOSXIntel64:
            case BuildTarget.StandaloneOSXUniversal:
                modifier = "_OSX";
                fileExtension = ".app";
                break;
            case BuildTarget.StandaloneLinux:
            case BuildTarget.StandaloneLinux64:
            case BuildTarget.StandaloneLinuxUniversal:
                modifier = "_LINUX";
                switch (buildTarget)
                {
                    case BuildTarget.StandaloneLinux: fileExtension = ".x86"; break;
                    case BuildTarget.StandaloneLinux64: fileExtension = ".x64"; break;
                    case BuildTarget.StandaloneLinuxUniversal: fileExtension = ".x86_64"; break;
                }
                break;
        }

        //____BUILDING PLAYER
        Debug.Log(string.Format("[UnityBuilder]-->({0})", buildTarget));
        //Swap active build target
        EditorUserBuildSettings.SwitchActiveBuildTarget(buildTarget);
        //Create path & file paths
        string buildPath = path +"/" + filename + modifier + dateSuffix + "/";
        string playerPath = buildPath + filename + modifier + fileExtension;
        Directory.CreateDirectory(buildPath);
        //Get build scenes
        string[] scenePaths = GetScenePaths();
        var buildOption = (buildTarget == BuildTarget.StandaloneWindows)
            ? BuildOptions.ShowBuiltPlayer
            : BuildOptions.None;
        //Build player
        BuildPipeline.BuildPlayer(scenePaths, playerPath, buildTarget, buildOption);
        Debug.Log(string.Format("[UnityBuilder]-->({0}) Saving Build to '{1}'", buildTarget, playerPath));
        //____ZIP & EXPORT
        if (!exportZip) return;
        var exportPath = ReadSettings().ExportPath;
        string zipOutput;
        //If export path non-existent, leave zips in output folder. This should probably not happen, but just in case.
        if (exportPath.Length < 2)
        {
            zipOutput = path + filename + modifier + dateSuffix + ".zip";
        }
        else //otherwise send to export path
        {
            zipOutput = exportPath + filename + modifier + dateSuffix + ".zip";
        }
        Debug.Log(string.Format("[UnityBuilder]-->({0}) Exporting .zip to '{1}'",buildTarget, exportPath));
        // ZIP everything
        _archiveFileName = filename + modifier + dateSuffix + ".zip";
        CompressDirectory(buildPath, zipOutput);
    }

    static string[] GetScenePaths()
    {
        var scenes = new string[EditorBuildSettings.scenes.Length];
        for (var i = 0; i < scenes.Length; i++)
        {
            scenes[i] = EditorBuildSettings.scenes[i].path;
        }
        return scenes;
    }

    public static string PromptForBuildSaveFolder()
    {
        var path = EditorUtility.SaveFolderPanel("Build Files to...", GetAssetsFolderPath() + "/Builds/", "");
        if (path == "") return ReadSettings().BuildPath;
        return path+"/";
    }

    static string GetAssetsFolderPath()
    {
        var s = Application.dataPath;
        s = s.Substring(s.Length - 7, 7);
        return s;
    }

    static void ClearStagingArea()
    {
        var path = Application.dataPath.Replace("/Assets", "/Temp/StagingArea/");
        
        if (!Directory.Exists(path)) return;
        
        Directory.Delete(path, true);
        Directory.CreateDirectory(path);
    }

    // compress the folder into a ZIP file, uses https://github.com/r2d2rigo/dotnetzip-for-unity
    static void CompressDirectory(string directory, string zipFileOutputPath)
    {
        using (var zip = new ZipFile())
        {
            zip.ParallelDeflateThreshold = -1; // DotNetZip bugfix that corrupts DLLs / binaries http://stackoverflow.com/questions/15337186/dotnetzip-badreadexception-on-extract
            zip.SaveProgress += SaveProgress;
            zip.AddDirectory(directory);
            zip.Save(zipFileOutputPath);
        }
    }

    private static string _archiveFileName;
    public static void SaveProgress(object sender, SaveProgressEventArgs e)
    { 
        //Started
        if (e.EventType == ZipProgressEventType.Saving_Started)
        {
            Debug.Log("[UnityBuilder]--> Compressing Started: " + e.ArchiveName);
        }   

        //Update Progress bar
        var bytesTransfered = e.BytesTransferred/(0.01*e.TotalBytesToTransfer);
        var barTitle = string.Format("Compressing Files...({0})", _archiveFileName);
        var barInfo = (e.CurrentEntry != null) ? string.Format("{0}", e.CurrentEntry.FileName) : e.ArchiveName;
        EditorUtility.DisplayProgressBar(barTitle, barInfo, (float)bytesTransfered / 100f);

        //Save complete
        if (e.EventType == ZipProgressEventType.Saving_Completed)
        {
            Debug.Log("[UnityBuilder]--> Compressing Complete: " + e.ArchiveName);
            EditorUtility.ClearProgressBar();
        }
    }
}