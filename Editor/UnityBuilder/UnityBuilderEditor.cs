﻿using UnityEngine;
using UnityEditor;

public class UnityBuilderEditor : EditorWindow
{
    private GUIStyle _guiStyle;
    public UnityBuilderSettings Settings;

    [MenuItem("Utilities/UnityBuilder/Unity Builder Wizard")]
    public static void ShowWindow()
    {
        var window = (UnityBuilderEditor) GetWindow(typeof(UnityBuilderEditor));
        window.titleContent = new GUIContent("Unity Builder");
        window.minSize = new Vector2(160f, 300f);
        window.maxSize = window.minSize * 1.1f;
        window.Show();
        window.Focus();
    }

    public void OnEnable()
    {
        //Setup GUIStyle for labels
        _guiStyle = new GUIStyle
        {
            normal = {textColor = Color.black},
            fontStyle = FontStyle.Bold,
            fontSize = 12,
            padding = new RectOffset(5,5,1,1),
            wordWrap = true
        };

        Settings = UnityBuilder.ReadSettings();

    }

    private void OnGUI()
    {
        if (Settings == null) Settings = new UnityBuilderSettings();

        EditorGUILayout.Space();
        EditorGUILayout.BeginVertical();
//HEADER
        GUILayout.Label("UnityBuilder Wizard", _guiStyle);
        EditorGUILayout.Space();

//PLATFORM TOGGLES
        Settings.BuildWin = EditorGUILayout.ToggleLeft("Windows", Settings.BuildWin);
        EditorGUILayout.Separator();
        Settings.BuildMac = EditorGUILayout.ToggleLeft("Mac/OSX", Settings.BuildMac);
        EditorGUILayout.Separator();
        Settings.BuildLinux = EditorGUILayout.ToggleLeft("Linux", Settings.BuildLinux);
        EditorGUILayout.Separator();
//SET BUILD PATH
        if (GUILayout.Button("Set Build Path", GUILayout.Width(150)))
        {
            //Set build path
            Settings.BuildPath = UnityBuilder.PromptForBuildSaveFolder();
        }
        EditorGUILayout.LabelField("Saving Builds To:\n" + Settings.BuildPath, GUILayout.Height(50));
        EditorGUILayout.Space();
//ZIP TOGGLE & GROUP
        Settings.ExportZip = EditorGUILayout.BeginToggleGroup("Export as ZIP", Settings.ExportZip);
        EditorGUILayout.Space();
//SET EXPORT PATH
        if (GUILayout.Button("Set Export Path", GUILayout.Width(150)))
        {
            //Set export path
            var zipPath = UnityBuilder.PromptForZipExportPath();
            if (zipPath != "") Settings.ExportPath = zipPath;
        }
        EditorGUILayout.LabelField("Exporting .Zip(s) To:\n" + Settings.ExportPath, GUILayout.Height(30));
        EditorGUILayout.Space();
        EditorGUILayout.EndToggleGroup();
        EditorGUILayout.Space();

//BUILD BUTTON
        if (GUILayout.Button("Build", GUILayout.Width(150)))
        {
            //Write settings
            UnityBuilder.WriteSettings(Settings);
            //Do builds
            UnityBuilder.MakeBuilds();
        }
        EditorGUILayout.EndVertical();
    }

    void OnDestroy()
    {
        //Save settings
        UnityBuilder.WriteSettings(Settings);
    }
}